# FlipCards

## Tech Stack

- [firebase]()
  - Auth
  - Functions
  - Hosting
  - Firestore
- [vue](https://vuejs.org/)
  - v3?
- Audio?

## Database Schema Ideas

- set
  - id: id
  - title: string
  - list of cards: Array[cards]
- card
  - id: id
  - term (front side): String
  - definition (back side): String
  - setId: id
  - memorized: Boolean

## Api requests

- User
  - Create account
  - Login
  - Edit account
  - Delete account
- Sets/Cards
  - Create Set
  - Create Card
  - Create Set With Cards
  - Get All Sets
  - Get Single Set
  - Get Sets Cards
  - Get All Cards
  - Get Single Card
  - Edit Set
  - Edit Card
  - Delete Set
  - Delete Card
  - Some Extras
    - Copy Cards
    - Move Cards
    - Merge Sets

## Docs

- Security (blocking functions).
  - https://cloud.google.com/identity-platform/docs/blocking-functions
  - Common Scenarios: https://cloud.google.com/identity-platform/docs/blocking-functions#common_scenarios
    - One that would be cool to try is requiring email verification on registration
- Environments
  - https://firebase.google.com/docs/projects/dev-workflows/overview-environments
- Emulator Suites (Could be a good way for testing)
  - https://firebase.google.com/docs/emulator-suite?authuser=1

## Checklists

- Firebase Launch Checklist: https://firebase.google.com/support/guides/launch-checklist
- Firebase Security Checklist: https://firebase.google.com/support/guides/security-checklist

## Research

- Good document on `Should I query my Firebase database directly, or use Cloud Functions?`
  - https://medium.com/firebase-developers/should-i-query-my-firebase-database-directly-or-use-cloud-functions-fbb3cd14118c

## Tutorials

- Vue
  - Vue 2: https://www.youtube.com/playlist?list=PL4cUxeGkcC9gQcYgjhBoeQH7wiAyZNrYa
  - Vuetify: https://www.youtube.com/playlist?list=PL4cUxeGkcC9g0MQZfHwKcuB0Yswgb3gA5
  - Vuex: https://www.youtube.com/playlist?list=PL4cUxeGkcC9i371QO_Rtkl26MwtiJ30P2
  - Vue CLI v3: https://www.youtube.com/playlist?list=PL4cUxeGkcC9iCKx06qSncuvEPZ7x1UnKD
